#!/bin/bash

# Set variables for commands 
build_file=$1
api_build_file="/home/ubuntu/devops"
api_build_src="${build_file}.tar.gz"
api_build_dest="/home/ubuntu/devops"
user="ubuntu"
check_api=false
check_script_src="/home/ubuntu/devops/check_api_mock.sh"
check_script_dest="/home/ubuntu/devops/check_api_mock.sh"

#Backup previous build 
cp -r $api_build_file/build $api_build_file/$EPOCHSECONDS

# Copy API build to the destination
chown -R "$user" "$api_build_dest"
chgrp -R "$user" "$api_build_dest"
chmod -R 775 "$api_build_dest"

# Decompress API build archive
tar -zxvf "$api_build_dest/${build_file}.tar.gz" -C "$api_build_dest"

# Copy API check script to the remote system
if [ "$check_api" == "true" ]; then
    cp "$check_script_src" "$check_script_dest"
    chown "$user" "$check_script_dest"
    chgrp "$user" "$check_script_dest"
    chmod 774 "$check_script_dest"
fi

# Sleep before running API validation script (adjust sleep time as needed)
if [ "$check_api" == "true" ]; then
    sleep 30
    chmod +x "$check_script_dest"
    bash "$check_script_dest"
fi

echo "Upload Build on S3 bucket"
#Save Build on S3 bucket
aws s3 cp $api_build_src s3://devops-node-api/

# delete old tar file from Instance 
rm -rf $api_build_src
