#!/bin/bash

# Description: Bash script that checks file consistency across multiple instances, retrieves the environment value, and notifies an external system.

# Define variables
region="us-east-1"
instances_list=("i-0880b3aeb3b2b55e4" "i-00acd76f160091729")
search_pattern="index"
notification_url="http://54.88.177.151:8065/hooks/c66ijhypipfhffejcx6wdq4qao"
errors=()
success_messages=()

# Check if there are instance IDs
if [ ${#instances_list[@]} -eq 0 ]; then
    echo "No instance IDs provided!"
    exit 1
fi

# Connect and fetch file names from each instance
for instance_id in "${instances_list[@]}"; do
    command_output=$(aws ssm send-command --instance-ids "$instance_id" \
    --document-name "AWS-RunShellScript" --region "$region" --parameters "commands=[\"ls /home/ubuntu/devops/build/ | grep $search_pattern\"]" \
    --query "Command.CommandId")
    ssm_command_output+=("$command_output")
    sleep 60  # Pause for SSM command execution
done


# Fetch results from the SSM command for each instance
for i in "${!instances_list[@]}"; do
    instance_id="${instances_list[$i]}"
    command_id="$(echo "${ssm_command_output[$i]}" | tr -d '"')"
    output=$(aws ssm list-command-invocations --instance-id "$instance_id" \
    --command-id "$command_id" --query "CommandInvocations[0].CommandPlugins[0].Output"  --region "$region")
    ssm_output+=("$output")
    echo $output
done

# Assert file names consistency
unique_file_names=($(echo "${ssm_output[@]}" | tr ' ' '\n' | sort -u))
if [ ${#unique_file_names[@]} -ne 1 ]; then
    echo "File names are not consistent across instances!"
    errors+=("File names are not consistent across instances!")
fi

# Notify external system
if [ ${#errors[@]} -eq 0 ]; then
    success_messages+=("Success! Git hash files are the same across the servers.")
fi
time=$(date)
# Notify external system
if [ ${#errors[@]} -eq 0 ]; then
    body="{
        \"attachments\": [
            {
            \"color\": \"#00FF00\",
            \"title\": \"Files Consistency Status\",
            \"text\": \"Date: ${time}\\nStatus: **${success_messages[*]}**\",
            \"footer\": \"Sent by:DevOps-Test\",
            \"ts\": $(date +%s)
            }
        ]
    }"
else
    body="{
        \"attachments\": [
            {
            \"color\": \"#FF0000\",
            \"title\": \"Files Consistency Status\",
            \"text\": \"Date: ${time}\\nStatus: **${errors[*]}**\",
            \"footer\": \"Sent by:DevOps-Test\",
            \"ts\": $(date +%s)
            }
        ]
    }"
fi

curl -X POST -H "Content-Type: application/json" -d "$body" "$notification_url"
