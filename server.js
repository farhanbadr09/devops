const express = require("express")
const packageJson = require('./package.json');

const app = express()

app.get('/', async(req, res)=>{
    res.send("Hello From Node Api")
})

app.get('/api/version', (req, res) => {
    res.json({ version: packageJson.version });
});

app.listen(4000, ()=>{
    console.log("Listening on port 4000")
})